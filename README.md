# codemirror-demo
Demo project to test out codemirror and webpack setup.

## Getting started

```
# to install dependencies
npm i

# run dev server with hot module replacement
npm run dev

# run bundler to create production build (in dist directory)
npm run build

```
